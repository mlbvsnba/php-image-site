<?php
error_reporting(E_ALL | E_STRICT);

require_once 'include/FileSettingsInterface.php';

$test = new FileSettingsInterface("settings.txt");

printSettings($test);

$test->set("name", "michael");

printSettings($test);

function printSettings($interface) {
    echo "<br /><br />Settings Below<br />" . nl2br($interface) . "<br />Settings Above<br />";
}

?>
