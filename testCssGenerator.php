<?php
error_reporting(E_ALL | E_STRICT);
require_once 'include/CssGenerator.php';
$gen = new CssGenerator();
$gen->setRule("div#centerText", "text-align", "center");
$gen->setRule("", "color", "black");
$gen->setRule("body", "margin", "auto");
$gen->saveCss("test.css");
//echo "Start CSS<br />", nl2br($gen), "<br />End CSS";

?>
