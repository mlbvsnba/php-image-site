<?php

require_once 'CssGenerator.php';

class CssParser extends CssGenerator {

    private $filePath, $noError = true;

    public function __construct($filePath) {
        $this->filePath = $filePath;
        //returns FALSE if the file doesn't exist
        if (!file_exists($filePath)) {
            $this->noError = false;
            return false;
        } else {
            $this->parseFile();
        }
    }

    /*
     * Reads the file line by line and sends them to parseLine
     */

    private function parseFile() {
        if (!$this->noError)
            return false;
        //opens file with read rights
        $fileHandle = fopen($this->filePath, 'r');
        //goes line by line until the eof
        while (!feof($fileHandle)) {
            $this->parseLine(trim(fgets($fileHandle)));
        }
    }

    /*
     * Recieves single lines
     * ignores empty lines
     * handles element lines (ex: "div {") and property lines
     * adds properties to CssGenerator's setRule
     */

    private function parseLine($line) {
        if (empty($line))
            return;
        $position = strpos($line, "{");
        //if "{" exists in the line
        if ($position !== FALSE) {
            parent::setElement(substr($line, 0, -1));
            return;
        }
        $position = strpos($line, ":");
        //if ":" exists in the line
        if ($position !== FALSE) {
            //seperate property and rule into array
            $rule = explode(":", $line, 2);
            //remove semi-colon from property's value
            $rule[1] = str_replace(';', '', $rule[1]);
            parent::setRule("", trim($rule[0]), trim($rule[1]));
        }
    }

    /*
     * Saves CSS to file using parent's saveCss
     * if filePath is empty, then uses original file path
     */

    public function saveCss($filePath = "") {
        if (empty($filePath))
            $filePath = $this->filePath;
        parent::saveCss($filePath);
    }

}

?>
