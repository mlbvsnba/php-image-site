<?php

require_once 'SettingsInterface.php';

class FileSettingsInterface extends SettingsInterface {

    private $settingDelim = array(" = ", ": ");
    private $filePath, $noError = true;

    public function __construct($filePath) {
        $this->filePath = $filePath;
        $this->initSettings();
    }

    private function openHandle() {
        $handle = fopen($this->filePath, "c+");
        if (!$handle)
            $noError = false;
        return $handle;
    }

    protected function initSettings() {
        $handle = $this->openHandle();
        if (!$this->noError)
            return false;
        while (!feof($handle)) {
            $this->parseSetting(trim(fgets($handle)));
        }
        fclose($handle);
    }

    private function parseSetting($line) {
        if (trim($line) == "")
            return false;
        foreach ($this->settingDelim as $delim) {
            if (strpos($line, $delim) !== FALSE) {
                $settingParts = explode($delim, $line, 2);
                parent::add(trim($settingParts[0]), trim($settingParts[1]));
                return true;
            }
        }
        return false;
    }

    protected function update($label, $value, $existed) {
        if (!$this->noError)
            return false;
        if (!$existed) {
            file_put_contents($this->filePath, "\n$label: $value", FILE_APPEND);
        } else {
            file_put_contents($this->filePath, parent::__toString());
        }
    }

    public function noError() {
        return $this->noError;
    }

    public function addDelim($delim) {
        if (!in_array($delim, $this->settingDelim)) {
            $this->settingDelim[] = $delim;
            $this->initSettings();
        }
    }

    public function getDelims() {
        return $this->settingDelim;
    }

}

?>