<?php

class CssGenerator {
    const TAB = "    ";

    private $rules = array(array()), $currentElement;

    /*
     * Adds a rule to the $rules array
     * - uses $currentElement if $element is empty
     * - saves $element to $currentElement if it is given
     */

    public function setRule($element, $property, $value) {
        if (empty($element) && !isset($this->currentElement)) {
            return false;
        } elseif (!empty($element)) {
            $this->currentElement = $element;
        }
        $this->rules[$this->currentElement][$property] = $value;
    }

    public function setElement($element) {
        $this->currentElement = trim($element);
    }

    /*
     * Returns true if the element and/or property exists
     * - if property is empty, then just looks for element
     */

    public function exists($element, $property = "") {
        if (empty($property) && empty($value)) {
            return key_exists($element, $this->rules);
        } else {
            if ($this->exists($element)) {
                return key_exists($property, $this->rules[$element]);
            } else
                return false;
        }
    }

    public function getValue($element, $property) {
        if ($this->exists($element, $property)) {
            return $this->rules[$element][$property];
        } else
            return "";
    }

    /*
     * Returns a string of formatted CSS
     */

    public function getCss() {
        $genCss = "";
        foreach ($this->rules as $element => $propertyArray) {
            //compensate for "0" key - why does it exist?!?!
            if ($element === 0)
                continue;
            $genCss .= "$element {\n";
            foreach ($propertyArray as $property => $value) {
                $genCss .= self::TAB . "$property: $value;\n";
            }
            $genCss .= "}\n\n";
        }
        //removes trailing "\n" from last element
        return rtrim($genCss);
    }

    /*
     * Saves the output of getCss to a file
     * - ALWAYS creates or overwrites the file
     * - returns output of file_put_contents
     */

    public function saveCss($filePath) {
        return file_put_contents($filePath, $this->getCss());
    }
    
    /*
     * Returns getCss if this object is used as a string
     */

    public function __toString() {
        return $this->getCss();
    }

}

?>