<?php

abstract class SettingsInterface {
    const StringDelim = " = ";

    private $settings = array();

    abstract protected function initSettings();

    abstract protected function update($label, $value, $existed);

    abstract public function __construct();

    final protected function add($label, $value) {
        $this->settings[$label] = $value;
    }

    final public function exists($label) {
        return array_key_exists($label, $this->settings);
    }

    final public function get($label) {
        if (!exists($label))
            return null;
        return $this->settings[$label];
    }
    
    final public function getAll() {
        return $this->settings;
    }

    final public function set($label, $value) {
        $existed = $this->exists($label);
        $this->settings[$label] = $value;
        $this->update($label, $value, $existed);
    }

    public function __toString() {
        $settingsString = "";
        foreach ($this->settings as $label => $value) {
            $settingsString .= $label . self::StringDelim . $value . "\n";
        }
        return rtrim($settingsString);
    }

//    protected function resetArray() {
//        unset($this->settings);
//        $this->settings = array();
//    }
}

?>
