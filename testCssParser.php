<?php

require_once 'include/CssParser.php';
$parser = new CssParser("test.css");
$parser->setRule("div#centerText", "color", "yellow");
$parser->setRule("", "display", "none");
$parser->setRule("", "text-align", "left");
$parser->saveCss();

?>
